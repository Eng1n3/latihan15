const express = require("express");
const app = express();
const expressLayouts = require("express-ejs-layouts");
const port = 8000;

app.set("view engine", "ejs");
app.use(expressLayouts);

app.get("/", (req, res) => {
	const mahasiswa = [
		{
			nama: "Adam Brilian",
			jurusan: "Tehnik Infomatika"
		},
		{
			nama: "C",
			jurusan: "Design UI/UX",
		}
	]
	res.render("index", {
		layout: "./layouts/base",
		title: "Home",
		nama: "Adam",
		mahasiswa
	});
});

app.get("/about", (req, res) => {
	res.render("about", {
		layout: "./layouts/base",
		title: "About",
	});
});

app.get("/contact", (req, res) => {
	res.render("contact", {
		layout: "./layouts/base",
		title: "Contact",
	});
});

app.listen(port, () => {
	console.log(`Server is listening on port: ${port}`)
})
